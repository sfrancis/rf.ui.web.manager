'use strict';

/**
 * <copyright file="app.js" company="GEODIS">
 * Copyright (c) 2016 GEODIS.
 * All rights reserved. www.geodis.com
 * Reproduction or transmission in whole or in part, in any form or by
 * any means, electronic, mechanical or otherwise, is prohibited without the
 * prior written consent of the copyright owner.
 * </copyright>
 * <author>SFrancis</author>
 * <date>09-21-2018</date>
 */

const fastify = require('fastify')();
const got = require('got');

const maxLength = 200;
const fileconfig = require('./config/fileconfig');
const deepStreamHelper = require('./helpers/deepstreamhelper');
const pm2MetricsCollector = require('./helpers/pm2metricscollector');
const grafanaController = require('./controllers/grafanacontroller');
global.servers = require('./config/servers');

global.pm2Metrics = {};
global.globalProcessMetrics = {
  averageCpu: [],
  averageMemory: [],
  totalUsers: [],
  averageLoopDelay: [],
  averageNetworkUpload: [],
  averageNetworkDownload: [],
  averageActiveHandles: [],
  averageActiveRequests: [],
};
global.appConfig = fileconfig[process.env.NODE_ENV];
global.deepStreamClient = deepStreamHelper.login();
const updatePm2Metrics = () => {
  pm2MetricsCollector.collectPm2Metrics();
};
deepStreamHelper.initializeMetricsLists(() => {
  updatePm2Metrics();
  // setTimeout(updatePm2Metrics, 10000);
});


fastify.route({
  method: 'POST',
  url: '/security/query',
  beforeHandler: async () => {
    // TODO: check authentication
  },
  handler: grafanaController.query,
});

fastify.route({
  method: 'GET',
  url: '/',
  beforeHandler: async () => {
    // TODO: check authentication
  },
  handler: async (request, reply) => {
    reply.code(200).send({ status: 'ok' });
  },
});


fastify.route({
  method: 'POST',
  url: '*',
  beforeHandler: async () => {
    // TODO: check authentication
  },
  handler: async (request) => {
    try {
      let options = {
        baseUrl: 'http://' + request.body.hostname + ':' + 37027,
        json: true,
        method: 'POST',
        timeout: 5000,
        body: { Key: request.body.Key, Arguments: request.body.Arguments, pid: request.body.pid },
      };
      const response = await got(request.params['*'], options);
      return response.body;
    } catch (error) {
      console.error(error);
      return error;
    }
  },
});

const start = async () => {
  try {
    const host = global.appConfig.host || '0.0.0.0';
    const port = global.appConfig.port || 37000;
    await fastify.listen(port, host, (err, address) => {
      console.log(`RF UI Web Interface Manager listening on ${address}`);
    });
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

// const getServerPm2Metrics = async (server) => {
//   try {
//     const pm2Metrics = await got('http://' + server + ':' + 9615);
//     global.pm2Metrics[server] = (JSON.parse(pm2Metrics.body).processes.filter(proc => [
//       'rf-app',
//       'rf-app-blue',
//       'rf-app-green',
//     ].includes(proc.name))) || global.pm2Metrics[server];
//   } catch (error) {
//     console.error(error);
//   }
// };
// const sliceGlobalMetrics = () => {
//   if (global.globalProcessMetrics.averageCpu.length > maxLength) {
//     global.globalProcessMetrics.averageCpu.splice(0, 1);
//   }
//   if (global.globalProcessMetrics.averageMemory.length > maxLength) {
//     global.globalProcessMetrics.averageMemory.splice(0, 1);
//   }
//   if (global.globalProcessMetrics.totalUsers.length > maxLength) {
//     global.globalProcessMetrics.totalUsers.splice(0, 1);
//   }
//   if (global.globalProcessMetrics.averageLoopDelay.length > maxLength) {
//     global.globalProcessMetrics.averageLoopDelay.splice(0, 1);
//   }
//   if (global.globalProcessMetrics.averageNetworkUpload.length > maxLength) {
//     global.globalProcessMetrics.averageNetworkUpload.splice(0, 1);
//   }
//   if (global.globalProcessMetrics.averageNetworkDownload.length > maxLength) {
//     global.globalProcessMetrics.averageNetworkDownload.splice(0, 1);
//   }
//   if (global.globalProcessMetrics.averageActiveHandles.length > maxLength) {
//     global.globalProcessMetrics.averageActiveHandles.splice(0, 1);
//   }
//   if (global.globalProcessMetrics.averageActiveRequests.length > maxLength) {
//     global.globalProcessMetrics.averageActiveRequests.splice(0, 1);
//   }
// };

// const updateGlobalMetrics = () => {
//   const globalProcessMetrics = Object.values(global.pm2Metrics);
//   let totalProcessCount = 0;
//   let realTimeUsers = 0;
//   let totalMemory = 0;
//   let totalCPU = 0;
//   let totalActiveHandles = 0;
//   let totalActiveRequests = 0;
//   let totalLoopDelay = 0;
//   let totalNetworkUpload = 0;
//   let totalNetworkDownload = 0;

//   for (const serverProcessMetrics of globalProcessMetrics) {
//     for (const processMetrics of serverProcessMetrics) {
//       realTimeUsers += processMetrics.pm2_env.axm_monitor['Real Time Users'].value;
//       totalMemory += processMetrics.monit.memory;
//       totalCPU += processMetrics.monit.cpu;
//       totalActiveHandles += processMetrics.pm2_env.axm_monitor['Active handles'].value;
//       totalActiveRequests += processMetrics.pm2_env.axm_monitor['Active requests'].value;
//       totalLoopDelay += Number(processMetrics.pm2_env.axm_monitor['Loop delay'].value.replace('ms', ''));
//       totalNetworkUpload += Number(processMetrics.pm2_env.axm_monitor['Network Upload'].value.replace('MB/s', ''));
//       totalNetworkDownload += Number(processMetrics.pm2_env.axm_monitor['Network Download'].value.replace('MB/s', ''));
//       ++totalProcessCount;
//     }
//   }
//   global.globalProcessMetrics.averageCpu.push([Math.round((totalCPU / totalProcessCount)), (new Date()).getTime()]);
//   global.globalProcessMetrics.averageMemory.push([(totalMemory / (totalProcessCount * 1000000)), (new Date()).getTime()]);
//   global.globalProcessMetrics.totalUsers.push([realTimeUsers, (new Date()).getTime()]);
//   global.globalProcessMetrics.averageLoopDelay.push([(totalLoopDelay / totalProcessCount), (new Date()).getTime()]);
//   global.globalProcessMetrics.averageNetworkUpload.push([(totalNetworkUpload / totalProcessCount), (new Date()).getTime()]);
//   global.globalProcessMetrics.averageNetworkDownload.push([(totalNetworkDownload / totalProcessCount), (new Date()).getTime()]);
//   global.globalProcessMetrics.averageActiveHandles.push([Math.round((totalActiveHandles / totalProcessCount)), (new Date()).getTime()]);
//   global.globalProcessMetrics.averageActiveRequests.push([Math.round((totalActiveRequests / totalProcessCount)), (new Date()).getTime()]);
//   sliceGlobalMetrics();
// };

// const getPm2Metrics = async () => {
//   const promises = global.servers.map(getServerPm2Metrics);
//   await Promise.all(promises);
//   updateGlobalMetrics();
// };

// getPm2Metrics();
// setInterval(getPm2Metrics, 5000);
start();
