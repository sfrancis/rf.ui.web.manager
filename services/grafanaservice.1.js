'use strict';

/**
 * <copyright file="grafanaservice.js" company="GEODIS">
 * Copyright (c) 2016 GEODIS.
 * All rights reserved. www.geodis.com
 * Reproduction or transmission in whole or in part, in any form or by
 * any means, electronic, mechanical or otherwise, is prohibited without the
 * prior written consent of the copyright owner.
 * </copyright>
 * <author>SFrancis</author>
 * <date>09-21-2018</date>
 */


const grafanaservice = {
  GlobalAverageCPU: async () => {
    const response = [{
      target: 'GlobalAverageCPU',
      datapoints: global.globalProcessMetrics.averageCpu,
    }];
    return response;
  },
  GlobalAverageMemory: async () => {
    const response = [{
      target: 'GlobalAverageMemory',
      datapoints: global.globalProcessMetrics.averageMemory,
    }];
    return response;
  },
  GlobalUsers: async () => {
    const response = [{
      target: 'GlobalUsers',
      datapoints: global.globalProcessMetrics.totalUsers,
    }];
    return response;
  },
  GlobalAverageLoopDelay: async () => {
    const response = [{
      target: 'GlobalAverageLoopDelay',
      datapoints: global.globalProcessMetrics.averageLoopDelay,
    }];
    return response;
  },
  GlobalAverageNetworkUpload: async () => {
    const response = [{
      target: 'GlobalAverageNetworkUpload',
      datapoints: global.globalProcessMetrics.averageNetworkUpload,
    }];
    return response;
  },
  GlobalAverageNetworkDownload: async () => {
    const response = [{
      target: 'GlobalAverageNetworkDownload',
      datapoints: global.globalProcessMetrics.averageNetworkDownload,
    }];
    return response;
  },
  GlobalAverageActiveRequests: async () => {
    const response = [{
      target: 'GlobalAverageActiveRequests',
      datapoints: global.globalProcessMetrics.averageActiveRequests,
    }];
    return response;
  },
  GlobalAverageActiveHandles: async () => {
    const response = [{
      target: 'GlobalAverageActiveHandles',
      datapoints: global.globalProcessMetrics.averageActiveHandles,
    }];
    return response;
  },
  ProcessOverview: async (options) => {
    try {
      const server = options.filter(option => option.Key.toLowerCase() === 'server')[0].Value;
      const processMetrics = global.pm2Metrics[server];
      const processOverViewTable = [{
        type: 'table',
        columns: [
          { text: 'App name', type: 'string' },
          { text: 'Instances', type: 'string' },
          { text: 'Status', type: 'number' },
          { text: 'CPU', type: 'string' },
          { text: 'Memory', type: 'string' },
        ],
        rows: [],
      }];
      const appName = processMetrics[0].name;
      const memoryList = processMetrics.map(proc => proc.monit.memory);
      const memoryAvg = (((memoryList.reduce((a, b) => a + b, 0) / memoryList.length) / 1000000).toFixed(2)).toString() + ' MB';
      const cpuList = processMetrics.map(proc => proc.monit.cpu);
      const cpuAvg = (cpuList.reduce((a, b) => a + b, 0) / cpuList.length).toFixed(2).toString() + ' %';
      const instances = processMetrics.length.toString();
      const onlineStatusList = processMetrics.map(proc => proc.pm2_env.status === 'online');
      const status = onlineStatusList.length === processMetrics.length ? 100
        : onlineStatusList.length === 0 ? 0 : 50;
      const processOverViewData = [
        appName,
        instances,
        status,
        cpuAvg,
        memoryAvg,
      ];
      processOverViewTable[0].rows.push(processOverViewData);
      return processOverViewTable;
    } catch (error) {
      console.error(error);
      return error;
    }
  },
  ProcessList: async (options) => {
    try {
      const server = options.filter(option => option.Key.toLowerCase() === 'job')[0].Value;
      const processMetrics = global.pm2Metrics[server];
      const processListTable = [{
        type: 'table',
        columns: [
          { text: 'App name', type: 'string' },
          { text: 'PM2 id', type: 'string' },
          { text: 'Pid', type: 'string' },
          { text: 'Version', type: 'string' },
          { text: 'Mode', type: 'string' },
          { text: 'Status', type: 'number' },
          { text: 'Restart', type: 'string' },
          { text: 'CPU', type: 'string' },
          { text: 'Memory', type: 'string' },
          { text: 'Active handles', type: 'string' },
          { text: 'Active requests', type: 'number' },
          { text: 'Loop delay', type: 'string' },
          { text: 'Network upload', type: 'string' },
          { text: 'Network download', type: 'string' },
          { text: 'Real time users', type: 'number' },
        ],
        rows: [],
      }];
      for (const proc of processMetrics) {
        const appstatus = proc.pm2_env.status === 'online' ? 100 : 0;
        const memory = (proc.monit.memory / 1000000).toFixed(2);
        processListTable[0].rows.push([
          proc.name,
          proc.pm_id.toString(),
          proc.pid.toString(),
          (proc.pm2_env.axm_monitor['App Version'] || {}).value,
          proc.pm2_env.exec_mode.replace('_mode', ''),
          appstatus.toString(),
          proc.pm2_env.restart_time.toString(),
          proc.monit.cpu.toString() + '%',
          memory.toString() + ' MB',
          (proc.pm2_env.axm_monitor['Active handles'] || {}).value,
          (proc.pm2_env.axm_monitor['Active requests'] || {}).value,
          (proc.pm2_env.axm_monitor['Loop delay'] || {}).value,
          (proc.pm2_env.axm_monitor['Network Upload'] || {}).value,
          (proc.pm2_env.axm_monitor['Network Download'] || {}).value,
          (proc.pm2_env.axm_monitor['Real Time Users'] || {}).value,
        ]);
      }
      return processListTable;
    } catch (error) {
      console.error(error);
      return error;
    }
  },
};
module.exports = grafanaservice;
