'use strict';

/**
 * <copyright file="deepstreamhelper.js" company="GEODIS">
 * Copyright (c) 2016 GEODIS.
 * All rights reserved. www.geodis.com
 * Reproduction or transmission in whole or in part, in any form or by
 * any means, electronic, mechanical or otherwise, is prohibited without the
 * prior written consent of the copyright owner.
 * </copyright>
 * <author>SFrancis</author>
 * <date>11-21-2018</date>
 */

const _deepstream = require('deepstream.io-client-js');
const async = require('neo-async');

const deepStream = {
  login: () => {
    const deepstreamUrl = global.appConfig.deepstream.host + ':'
      + global.appConfig.deepstream.port +
      global.appConfig.deepstream.endpoint;
    const deepStreamClient = _deepstream(deepstreamUrl);
    deepStreamClient.on('error', (error, event, topic) => {
      console.log(error, event, topic);
    });
    deepStreamClient.login();
    return deepStreamClient;
  },
  initializeMetricsLists: (callback) => {
    const pm2MetricsRecord = global.deepStreamClient.record.getRecord('pm2metrics');
    const timeStampList = global.deepStreamClient.record.getList('timestamps');
    // const cpuUtilizationRecord = global.deepStreamClient.record.getRecord('cpu');
    // const memoryUtilizationRecord = global.deepStreamClient.record.getRecord('memory');
    // const noOfUsersRecord = global.deepStreamClient.record.getRecord('users');
    // const loopDelayRecord = global.deepStreamClient.record.getRecord('loopdelay');
    // const networkUploadRecord = global.deepStreamClient.record.getRecord('networkupload');
    // const networkDownloadRecord = global.deepStreamClient.record.getRecord('networkdownload');
    // const activeHandlesRecord = global.deepStreamClient.record.getRecord('activehandles');
    // const activeRequestsRecord = global.deepStreamClient.record.getRecord('activerequests');
    pm2MetricsRecord.set({ metrics: {} });
    timeStampList.setEntries([]);
    // cpuUtilizationRecord.set({ datapoints: {} });
    // noOfUsersRecord.set({ datapoints: {} });
    // noOfUsersRecord.set({ datapoints: {} });
    // loopDelayRecord.set({ datapoints: {} });
    // networkUploadRecord.set({ datapoints: {} });
    // networkDownloadRecord.set({ datapoints: {} });
    // activeHandlesRecord.set({ datapoints: {} });
    // activeRequestsRecord.set({ datapoints: {} });
    async.forEach(global.servers, (server, done) => {
      // const timeStampList = global.deepStreamClient.record.getList(server + '_timestamp');
      const cpuList = global.deepStreamClient.record.getList(server + '_cpu');
      const memoryList = global.deepStreamClient.record.getList(server + '_memory');
      const usersList = global.deepStreamClient.record.getList(server + '_users');
      const loopdelayList = global.deepStreamClient.record.getList(server + '_loopdelay');
      const networkuploadList = global.deepStreamClient.record.getList(server + '_networkupload');
      const networkdownloadList = global.deepStreamClient.record.getList(server + '_networkdownload');
      const activehandlesList = global.deepStreamClient.record.getList(server + '_activehandles');
      const activerequestsList = global.deepStreamClient.record.getList(server + '_activerequests');
      // timeStampList.setEntries([]);
      cpuList.setEntries([]);
      memoryList.setEntries([]);
      usersList.setEntries([]);
      loopdelayList.setEntries([]);
      networkuploadList.setEntries([]);
      networkdownloadList.setEntries([]);
      activehandlesList.setEntries([]);
      activerequestsList.setEntries([]);
      done();
    }, (err) => {
      console.error(err);
      callback();
    });
    // for (const server of global.servers) {
    // cpuUtilizationRecord.set('datapoints.' + server, []);
    // memoryUtilizationRecord.set('datapoints.' + server, []);
    // noOfUsersRecord.set('datapoints.' + server, []);
    // loopDelayRecord.set('datapoints.' + server, []);
    // networkUploadRecord.set('datapoints.' + server, []);
    // networkDownloadRecord.set('datapoints.' + server, []);
    // activeHandlesRecord.set('datapoints.' + server, []);
    // activeRequestsRecord.set('datapoints.' + server, []);

    // pm2MetricsRecord.whenReady(() => {
    //   callback();
    // });
    // }
  },
};
module.exports = deepStream;
