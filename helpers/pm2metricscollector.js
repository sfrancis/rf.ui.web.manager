'use strict';

/**
 * <copyright file="pm2metricscollector.js" company="GEODIS">
 * Copyright (c) 2016 GEODIS.
 * All rights reserved. www.geodis.com
 * Reproduction or transmission in whole or in part, in any form or by
 * any means, electronic, mechanical or otherwise, is prohibited without the
 * prior written consent of the copyright owner.
 * </copyright>
 * <author>SFrancis</author>
 * <date>11-21-2018</date>
 */

const got = require('got');
const async = require('neo-async');

const maxLength = 4;

const epoch = () => ((new Date()).getTime());
let timeStamp = epoch();
// const formatMetric = metric => metric.toString() + '_' + timeStamp;
const formatMetric = metric => metric.toString();


const pm2MetricsCollector = {
  collectPm2Metrics: async () => {
    const pm2MetricsDeepStreamRecord = global.deepStreamClient.record.getRecord('pm2metrics');
    const timeStampList = global.deepStreamClient.record.getList('timestamps');

    // const cpuUtilizationRecord = global.deepStreamClient.record.getRecord('cpu');
    // const memoryUtilizationRecord = global.deepStreamClient.record.getRecord('memory');
    // const noOfUsersRecord = global.deepStreamClient.record.getRecord('users');
    // const loopDelayRecord = global.deepStreamClient.record.getRecord('loopdelay');
    // const networkUploadRecord = global.deepStreamClient.record.getRecord('networkupload');
    // const networkDownloadRecord = global.deepStreamClient.record.getRecord('networkdownload');
    // const activeHandlesRecord = global.deepStreamClient.record.getRecord('activehandles');
    // const activeRequestsRecord = global.deepStreamClient.record.getRecord('activerequests');

    // const dicardOldestMetric = async () => {
    //   const dicardOldestMetricForServer = async (server) => {
    //     const cpuList = global.deepStreamClient.record.getList(server + '_cpu');
    //     const memoryList = global.deepStreamClient.record.getList(server + '_memory');
    //     const usersList = global.deepStreamClient.record.getList(server + '_users');
    //     const loopdelayList = global.deepStreamClient.record.getList(server + '_loopdelay');
    //     const networkuploadList = global.deepStreamClient.record.getList(server + '_networkupload');
    //     const networkdownloadList = global.deepStreamClient.record.getList(server + '_networkdownload');
    //     const activehandlesList = global.deepStreamClient.record.getList(server + '_activehandles');
    //     const activerequestsList = global.deepStreamClient.record.getList(server + '_activerequests');

    //     if (cpuDataPoints.length > maxLength) {
    //       cpuDataPoints.splice(0, 1);
    //       memoryDataPoints.splice(0, 1);
    //       nofUsersDataPoints.splice(0, 1);
    //       loopDelayDataPoints.splice(0, 1);
    //       networkUploadDataPoints.splice(0, 1);
    //       networkDownloadDataPoints.splice(0, 1);
    //       activeHandlesDataPoints.splice(0, 1);
    //       activeRequestsDataPoints.splice(0, 1);

    //       cpuUtilizationRecord.set('datapoints.' + server, cpuDataPoints);
    //       memoryUtilizationRecord.set('datapoints.' + server, memoryDataPoints);
    //       noOfUsersRecord.set('datapoints.' + server, nofUsersDataPoints);
    //       loopDelayRecord.set('datapoints.' + server, loopDelayDataPoints);
    //       networkUploadRecord.set('datapoints.' + server, networkUploadDataPoints);
    //       networkDownloadRecord.set('datapoints.' + server, networkDownloadDataPoints);
    //       activeHandlesRecord.set('datapoints.' + server, activeHandlesDataPoints);
    //       activeRequestsRecord.set('datapoints.' + server, activeRequestsDataPoints);
    //     }
    //   };
    //   const promises = global.servers.map(dicardOldestMetricForServer);
    //   await Promise.all(promises);
    // };

    const updatePm2Metrics = (callback) => {
      const globalProcessMetrics = Object.values(pm2MetricsDeepStreamRecord.get('metrics'));
      timeStamp = epoch();
      let timeStampDataPoints = timeStampList.getEntries();
      if (timeStampDataPoints.length > maxLength) {
        timeStampDataPoints.splice(0, 1);
      }
      timeStampDataPoints.push(formatMetric(timeStamp));
      timeStampList.setEntries(timeStampDataPoints);

      async.forEach(globalProcessMetrics, (serverProcessMetrics, done) => {
        const server = serverProcessMetrics[0].pm2_env.env.HOSTNAME.split('.')[0].toUpperCase();

        // const timeStampList = global.deepStreamClient.record.getList(server + '_timestamp');

        const cpuList = global.deepStreamClient.record.getList(server + '_cpu');
        const memoryList = global.deepStreamClient.record.getList(server + '_memory');
        const usersList = global.deepStreamClient.record.getList(server + '_users');
        const loopdelayList = global.deepStreamClient.record.getList(server + '_loopdelay');
        const networkuploadList = global.deepStreamClient.record.getList(server + '_networkupload');
        const networkdownloadList = global.deepStreamClient.record.getList(server + '_networkdownload');
        const activehandlesList = global.deepStreamClient.record.getList(server + '_activehandles');
        const activerequestsList = global.deepStreamClient.record.getList(server + '_activerequests');

        // let timeStampDataPoints = timeStampList.getEntries();
        let cpuDataPoints = cpuList.getEntries();
        let memoryDataPoints = memoryList.getEntries();
        let nofUsersDataPoints = usersList.getEntries();
        let loopDelayDataPoints = loopdelayList.getEntries();
        let networkUploadDataPoints = networkuploadList.getEntries();
        let networkDownloadDataPoints = networkdownloadList.getEntries();
        let activeHandlesDataPoints = activehandlesList.getEntries();
        let activeRequestsDataPoints = activerequestsList.getEntries();

        // if (timeStampDataPoints.length > maxLength) {
        //   timeStampDataPoints.splice(0, 1);
        // }
        if (cpuDataPoints.length > maxLength) {
          cpuDataPoints.splice(0, 1);
        }
        if (memoryDataPoints.length > maxLength) {
          memoryDataPoints.splice(0, 1);
        }
        if (nofUsersDataPoints.length > maxLength) {
          nofUsersDataPoints.splice(0, 1);
        }
        if (loopDelayDataPoints.length > maxLength) {
          loopDelayDataPoints.splice(0, 1);
        }
        if (networkUploadDataPoints.length > maxLength) {
          networkUploadDataPoints.splice(0, 1);
        }
        if (networkDownloadDataPoints.length > maxLength) {
          networkDownloadDataPoints.splice(0, 1);
        }
        if (activeHandlesDataPoints.length > maxLength) {
          activeHandlesDataPoints.splice(0, 1);
        }
        if (activeRequestsDataPoints.length > maxLength) {
          activeRequestsDataPoints.splice(0, 1);
        }

        let realTimeUsers = 0;
        let totalMemory = 0;
        let totalCPU = 0;
        let totalActiveHandles = 0;
        let totalActiveRequests = 0;
        let totalLoopDelay = 0;
        let totalNetworkUpload = 0;
        let totalNetworkDownload = 0;

        async.forEach(serverProcessMetrics, (processMetrics, innerDone) => {
          realTimeUsers += processMetrics.pm2_env.axm_monitor['Real Time Users'].value;
          totalMemory += processMetrics.monit.memory;
          totalCPU += processMetrics.monit.cpu;
          totalActiveHandles += processMetrics.pm2_env.axm_monitor['Active handles'].value;
          totalActiveRequests += processMetrics.pm2_env.axm_monitor['Active requests'].value;
          totalLoopDelay += Number(processMetrics.pm2_env.axm_monitor['Loop delay'].value.replace('ms', ''));
          totalNetworkUpload += Number(processMetrics.pm2_env.axm_monitor['Network Upload'].value.replace('MB/s', ''));
          totalNetworkDownload += Number(processMetrics.pm2_env.axm_monitor['Network Download'].value.replace('MB/s', ''));
          innerDone();
        }, () => {
          // timeStampDataPoints.push(formatMetric(timeStamp));
          cpuDataPoints.push(formatMetric(totalCPU));
          memoryDataPoints.push(formatMetric(totalMemory));
          nofUsersDataPoints.push(formatMetric(realTimeUsers));
          loopDelayDataPoints.push(formatMetric(totalLoopDelay));
          networkUploadDataPoints.push(formatMetric(totalNetworkUpload));
          networkDownloadDataPoints.push(formatMetric(totalNetworkDownload));
          activeHandlesDataPoints.push(formatMetric(totalActiveHandles));
          activeRequestsDataPoints.push(formatMetric(totalActiveRequests));


          // timeStampList.setEntries(timeStampDataPoints);
          cpuList.setEntries(cpuDataPoints);
          memoryList.setEntries(memoryDataPoints);
          usersList.setEntries(nofUsersDataPoints);
          loopdelayList.setEntries(loopDelayDataPoints);
          networkuploadList.setEntries(networkUploadDataPoints);
          networkdownloadList.setEntries(networkDownloadDataPoints);
          activehandlesList.setEntries(activeHandlesDataPoints);
          activerequestsList.setEntries(activeRequestsDataPoints);

          // console.log('*********************************** ' + server + '**********************************');
          // console.log('*** timeStamp ***');
          // console.log(timeStampList.getEntries());
          // console.log('*** CPU ***');
          // console.log(cpuList.getEntries());
          // console.log('*** Memory ***');
          // console.log(memoryList.getEntries());
          // console.log('*** Users ***');
          // console.log(usersList.getEntries());
          // console.log('*** Loop Delay ***');
          // console.log(loopdelayList.getEntries());
          // console.log('*** Network Upload ***');
          // console.log(networkuploadList.getEntries());
          // console.log('*** Network Download ***');
          // console.log(networkdownloadList.getEntries());
          // console.log('*** Active Handles ***');
          // console.log(activehandlesList.getEntries());
          // console.log('*** Active Requests ***');
          // console.log(activerequestsList.getEntries());
          // console.log('************************************************************************************');
          done();
        });
      }, () => {
        // dicardOldestMetric();
        callback();
      });
    };
    // const updatePm2Metrics1 = (callback) => {
    //   const globalProcessMetrics = Object.values(pm2MetricsDeepStreamRecord.get('metrics'));

    //   async.forEach(globalProcessMetrics, (serverProcessMetrics, done) => {
    //     const server = serverProcessMetrics[0].pm2_env.env.HOSTNAME.split('.')[0].toUpperCase();

    //     const cpuDataPoints = cpuUtilizationRecord.get('datapoints.' + server);
    //     const memoryDataPoints = memoryUtilizationRecord.get('datapoints.' + server);
    //     const nofUsersDataPoints = noOfUsersRecord.get('datapoints.' + server);
    //     const loopDelayDataPoints = loopDelayRecord.get('datapoints.' + server);
    //     const networkUploadDataPoints = networkUploadRecord.get('datapoints.' + server);
    //     const networkDownloadDataPoints = networkDownloadRecord.get('datapoints.' + server);
    //     const activeHandlesDataPoints = activeHandlesRecord.get('datapoints.' + server);
    //     const activeRequestsDataPoints = activeRequestsRecord.get('datapoints.' + server);

    //     let realTimeUsers = 0;
    //     let totalMemory = 0;
    //     let totalCPU = 0;
    //     let totalActiveHandles = 0;
    //     let totalActiveRequests = 0;
    //     let totalLoopDelay = 0;
    //     let totalNetworkUpload = 0;
    //     let totalNetworkDownload = 0;
    //     async.forEach(serverProcessMetrics, (processMetrics, innerDone) => {
    //       realTimeUsers += processMetrics.pm2_env.axm_monitor['Real Time Users'].value;
    //       totalMemory += processMetrics.monit.memory;
    //       totalCPU += processMetrics.monit.cpu;
    //       totalActiveHandles += processMetrics.pm2_env.axm_monitor['Active handles'].value;
    //       totalActiveRequests += processMetrics.pm2_env.axm_monitor['Active requests'].value;
    //       totalLoopDelay += Number(processMetrics.pm2_env.axm_monitor['Loop delay'].value.replace('ms', ''));
    //       totalNetworkUpload += Number(processMetrics.pm2_env.axm_monitor['Network Upload'].value.replace('MB/s', ''));
    //       totalNetworkDownload += Number(processMetrics.pm2_env.axm_monitor['Network Download'].value.replace('MB/s', ''));
    //       innerDone();
    //     }, (err) => {
    //       cpuDataPoints.push([totalCPU, epoch()]);
    //       memoryDataPoints.push([totalMemory, epoch()]);
    //       nofUsersDataPoints.push([realTimeUsers, epoch()]);
    //       loopDelayDataPoints.push([totalLoopDelay, epoch()]);
    //       networkUploadDataPoints.push([totalNetworkUpload, epoch()]);
    //       networkDownloadDataPoints.push([totalNetworkDownload, epoch()]);
    //       activeHandlesDataPoints.push([totalActiveHandles, epoch()]);
    //       activeRequestsDataPoints.push([totalActiveRequests, epoch()]);

    //       cpuUtilizationRecord.set('datapoints.' + server, cpuDataPoints);
    //       memoryUtilizationRecord.set('datapoints.' + server, memoryDataPoints);
    //       noOfUsersRecord.set('datapoints.' + server, nofUsersDataPoints);
    //       loopDelayRecord.set('datapoints.' + server, loopDelayDataPoints);
    //       networkUploadRecord.set('datapoints.' + server, networkUploadDataPoints);
    //       networkDownloadRecord.set('datapoints.' + server, networkDownloadDataPoints);
    //       activeHandlesRecord.set('datapoints.' + server, activeHandlesDataPoints);
    //       activeRequestsRecord.set('datapoints.' + server, activeRequestsDataPoints);

    //       console.log('*********************************** ' + server + '**********************************');
    //       console.log(cpuDataPoints);
    //       console.log(memoryDataPoints);
    //       console.log(nofUsersDataPoints);
    //       console.log(loopDelayDataPoints);
    //       console.log(networkUploadDataPoints);
    //       console.log(networkDownloadDataPoints);
    //       console.log(activeHandlesDataPoints);
    //       console.log(activeRequestsDataPoints);
    //       console.log('************************************************************************************');
    //       done();
    //     });
    //   }, (err) => {
    //     dicardOldestMetric();
    //     callback();
    //   });
    // };
    // const updateGlobalMetrics = () => {
    //   const globalProcessMetrics = Object.values(pm2MetricsDeepStreamRecord.get('metrics'));

    //   for (const serverProcessMetrics of globalProcessMetrics) {
    //     const server = serverProcessMetrics[0].pm2_env.env.HOSTNAME.split('.')[0].toUpperCase();

    //     const cpuDataPoints = cpuUtilizationRecord.get('datapoints.' + server);
    //     const memoryDataPoints = memoryUtilizationRecord.get('datapoints.' + server);
    //     const nofUsersDataPoints = noOfUsersRecord.get('datapoints.' + server);
    //     const loopDelayDataPoints = loopDelayRecord.get('datapoints.' + server);
    //     const networkUploadDataPoints = networkUploadRecord.get('datapoints.' + server);
    //     const networkDownloadDataPoints = networkDownloadRecord.get('datapoints.' + server);
    //     const activeHandlesDataPoints = activeHandlesRecord.get('datapoints.' + server);
    //     const activeRequestsDataPoints = activeRequestsRecord.get('datapoints.' + server);

    //     let realTimeUsers = 0;
    //     let totalMemory = 0;
    //     let totalCPU = 0;
    //     let totalActiveHandles = 0;
    //     let totalActiveRequests = 0;
    //     let totalLoopDelay = 0;
    //     let totalNetworkUpload = 0;
    //     let totalNetworkDownload = 0;

    //     for (const processMetrics of serverProcessMetrics) {
    //       realTimeUsers += processMetrics.pm2_env.axm_monitor['Real Time Users'].value;
    //       totalMemory += processMetrics.monit.memory;
    //       totalCPU += processMetrics.monit.cpu;
    //       totalActiveHandles += processMetrics.pm2_env.axm_monitor['Active handles'].value;
    //       totalActiveRequests += processMetrics.pm2_env.axm_monitor['Active requests'].value;
    //       totalLoopDelay += Number(processMetrics.pm2_env.axm_monitor['Loop delay'].value.replace('ms', ''));
    //       totalNetworkUpload += Number(processMetrics.pm2_env.axm_monitor['Network Upload'].value.replace('MB/s', ''));
    //       totalNetworkDownload += Number(processMetrics.pm2_env.axm_monitor['Network Download'].value.replace('MB/s', ''));
    //     }
    //     cpuDataPoints.push([totalCPU, epoch()]);
    //     memoryDataPoints.push([totalMemory, epoch()]);
    //     nofUsersDataPoints.push([realTimeUsers, epoch()]);
    //     loopDelayDataPoints.push([totalLoopDelay, epoch()]);
    //     networkUploadDataPoints.push([totalNetworkUpload, epoch()]);
    //     networkDownloadDataPoints.push([totalNetworkDownload, epoch()]);
    //     activeHandlesDataPoints.push([totalActiveHandles, epoch()]);
    //     activeRequestsDataPoints.push([totalActiveRequests, epoch()]);

    //     cpuUtilizationRecord.set('datapoints.' + server, cpuDataPoints);
    //     memoryUtilizationRecord.set('datapoints.' + server, memoryDataPoints);
    //     noOfUsersRecord.set('datapoints.' + server, nofUsersDataPoints);
    //     loopDelayRecord.set('datapoints.' + server, loopDelayDataPoints);
    //     networkUploadRecord.set('datapoints.' + server, networkUploadDataPoints);
    //     networkDownloadRecord.set('datapoints.' + server, networkDownloadDataPoints);
    //     activeHandlesRecord.set('datapoints.' + server, activeHandlesDataPoints);
    //     activeRequestsRecord.set('datapoints.' + server, activeRequestsDataPoints);

    //     console.log('*********************************** ' + server + '**********************************');
    //     console.log(cpuDataPoints);
    //     console.log(memoryDataPoints);
    //     console.log(nofUsersDataPoints);
    //     console.log(loopDelayDataPoints);
    //     console.log(networkUploadDataPoints);
    //     console.log(networkDownloadDataPoints);
    //     console.log(activeHandlesDataPoints);
    //     console.log(activeRequestsDataPoints);
    //     console.log('************************************************************************************');
    //   }
    //   dicardOldestMetric();
    // };

    const getServerPm2Metrics = async (server) => {
      try {
        const pm2Metrics = await got('http://' + server + ':' + 9615);
        const filteredPm2Metrics = (JSON.parse(pm2Metrics.body).processes.filter(proc => [
          'rf-app',
          'rf-app-blue',
          'rf-app-green',
        ].includes(proc.name)));
        // console.log(server);
        // console.log(filteredPm2Metrics);
        filteredPm2Metrics[0].pm2_env.env.HOSTNAME = server;
        pm2MetricsDeepStreamRecord.set('metrics.' + server, filteredPm2Metrics);
      } catch (error) {
        console.error(error);
      }
    };
    const getPm2Metrics = async (callback) => {
      const promises = global.servers.map(getServerPm2Metrics);
      await Promise.all(promises);
      // console.log(pm2MetricsDeepStreamRecord.get('metrics'));
      // updateGlobalMetrics();
      updatePm2Metrics(() => {
        callback();
      });
    };

    // getPm2Metrics(() => { });
    setTimeout(function request() {
      getPm2Metrics(() => {
        setTimeout(request, 5000);
      });
    }, 5000);
  },
};
module.exports = pm2MetricsCollector;

