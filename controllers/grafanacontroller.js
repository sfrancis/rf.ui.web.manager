'use strict';

/**
 * <copyright file="grafanacontroller.js" company="GEODIS">
 * Copyright (c) 2016 GEODIS.
 * All rights reserved. www.geodis.com
 * Reproduction or transmission in whole or in part, in any form or by
 * any means, electronic, mechanical or otherwise, is prohibited without the
 * prior written consent of the copyright owner.
 * </copyright>
 * <author>SFrancis</author>
 * <date>09-21-2018</date>
 */

const grafanaService = require('../services/grafanaservice');

const grafanacontroller = {
  query: async (request) => {
    try {
      return grafanaService[request.body.Key](request.body.Arguments);
    } catch (error) {
      console.error(error);
      return error;
    }
  },
};
module.exports = grafanacontroller;
